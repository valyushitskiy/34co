import Vue from "vue";
import Vuex from "vuex";
import { butter } from "@/buttercms";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    language: "ru",
    slideindex: 0,
    projects: [],
    currentproject: 0,
    collabs: [],
    currentcollab: 0
  },
  getters: {
    projectsByLang: state => () => {
      let projectsByLang = [];
      for (let i = 0; i < state.projects.length; i++) {
        const project = state.projects[i];
        if (project.fields.language === state.language) {
          projectsByLang.push(project);
        }
      }
      return projectsByLang;
    },
    collabsByLang: state => () => {
      let collabsByLang = [];
      for (let i = 0; i < state.collabs.length; i++) {
        const collab = state.collabs[i];
        if (collab.fields.language === state.language) {
          collabsByLang.push(collab);
        }
      }
      return collabsByLang;
    }
  },
  mutations: {
    setLang: (state, payload) => (state.language = payload),
    changeIndex: (state, payload) => (state.slideindex = payload),
    setProjects: (state, payload) => (state.projects = payload),
    setCurrent: (state, payload) => (state.currentproject = payload),
    setCollabs: (state, payload) => (state.collabs = payload),
    setCurrentCollab: (state, payload) => (state.currentcollab = payload)
  },
  actions: {
    getPages(context) {
      butter.page.list("project", { page_size: 1000 }).then(res => {
        const projects = res.data.data;
        context.commit("setProjects", projects);
      });
    },
    getCollabs(context) {
      butter.page.list("collab", { page_size: 1000 }).then(res => {
        const collabs = res.data.data;
        context.commit("setCollabs", collabs);
      });
    }
  }
});

export default store;
