import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "*",
      name: "notfound",
      component: () =>
        import(/* webpackChunkName: "notfound" */ "./views/NotFound.vue")
    },
    {
      path: "/projects",
      name: "projects",
      component: () =>
        import(/* webpackChunkName: "projects" */ "./views/Projects.vue")
    },
    {
      path: "/projects/:slug",
      name: "projectview",
      component: () =>
        import(/* webpackChunkName: "projectview" */ "./views/ProjectView.vue")
    },
    {
      path: "/service",
      name: "service",
      component: () =>
        import(/* webpackChunkName: "service" */ "./views/Service.vue")
    },
    {
      path: "/service/:slug",
      name: "serviceview",
      component: () =>
        import(/* webpackChunkName: "serviceview" */ "./views/ServiceView.vue")
    },
    {
      path: "/partners",
      name: "partners",
      component: () =>
        import(/* webpackChunkName: "partners" */ "./views/Partners.vue")
    },
    {
      path: "/team",
      name: "team",
      component: () => import(/* webpackChunkName: "team" */ "./views/Team.vue")
    },
    {
      path: "/contact",
      name: "contact",
      component: () =>
        import(/* webpackChunkName: "contact" */ "./views/Contact.vue")
    },
    {
      path: "/collaborations",
      name: "collaborations",
      component: () =>
        import(
          /* webpackChunkName: "collaborations" */ "./views/Collaborations.vue"
        )
    },
    {
      path: "/collaborations/:slug",
      name: "collabview",
      component: () =>
        import(/* webpackChunkName: "collabview" */ "./views/CollabView.vue")
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "/soon",
      name: "soon",
      component: () => import(/* webpackChunkName: "soon" */ "./views/Soon.vue")
    }
  ]
});
