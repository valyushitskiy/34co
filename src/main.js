import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import BootstrapVue from "bootstrap-vue";
import Vue2TouchEvents from "vue2-touch-events";
import Localize from "v-localize";

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(Vue2TouchEvents);
Vue.use(Localize);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
